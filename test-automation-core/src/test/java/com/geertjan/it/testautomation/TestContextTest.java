package com.geertjan.it.testautomation;

import com.geertjan.it.testautomation.driver.Runner;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertEquals;

public class TestContextTest {

    TestContext testContext;

    @Test
    public void testRunnerFirefox() throws Exception {
        performTests(Platform.FIREFOX);
    }

    @Test
    public void testRunnerChrome() throws Exception {
        System.setProperty("chrome.driver.binary", "/Users/GJDB/chromedriver");
        performTests(Platform.CHROME);
    }

    @Test
    public void testRunnerHeadless() throws Exception {
        System.setProperty("headless.driver.binary", "/Users/GJDB/phantomjs-2.0.0-macosx/bin/phantomjs");

        performTests(Platform.HEADLESS);
    }

    @Test
    public void testRunnerAndroid() throws Exception {
        performTests(Platform.ANDROID);
    }

    @Test
    public void testRunnerIos() throws Exception {
        performTests(Platform.IOS);
    }

    private void performTests(Platform platform) {
        testContext = new TestContext(platform, "");
        Runner runner = testContext.getRunner();
        runner.get("http://www.google.nl");

        assertEquals("Google", runner.getTitle());
        runner.takeAndSaveScreenshot();

        Actions action = new Actions(testContext.getRunner());

    }

    @After
    public void tearDown() {
        if(testContext != null) {
            testContext.doTearDown();
        }
    }
}