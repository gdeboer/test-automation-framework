package com.geertjan.it.testautomation.util;

import com.geertjan.it.testautomation.model.OSType;

import java.util.logging.Logger;

public class Locator {
    private static final Logger LOG = Logger.getLogger(Locator.class.getName());

    private final static String DRIVERS_LOCATION = "test-automation-drivers-1.0";

    public static String determineChromeDriverBinaryPath(String modulePath) {
        String systemProperty = System.getProperty("chrome.driver.binary");
        if (systemProperty == null || systemProperty.trim().length() == 0) {
            OSType osType = DetermineOS.getOperatingSystemType();

            switch (osType) {
                case WINDOWS:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/chromedriver-win32.exe";
                case MACOS:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/chromedriver-mac32";
                case LINUX:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/chromedriver-linux64";
                default:
                    LOG.severe("Unsupported operating system, provide the binary path yourself");
                    break;
            }

            return null;
        } else {
            LOG.info("Use the following binary for chrome testing " + systemProperty);
            return systemProperty;
        }
    }

    public static String determineIEDriverBinaryPath(String modulePath) {
        String systemProperty = System.getProperty("ie.driver.binary");
        if (systemProperty == null || systemProperty.trim().length() == 0) {
            OSType osType = DetermineOS.getOperatingSystemType();

            switch (osType) {
                case WINDOWS:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION +"/IEDriverServer-2-44-0.exe";
                case MACOS:
                    LOG.severe("Unsupported operating system");
                    break;
                case LINUX:
                    LOG.severe("Unsupported operating system");
                    break;
                default:
                    LOG.severe("Unsupported operating system");
                    break;
            }

            return null;
        } else {
            LOG.info("Use the following binary for ie testing " + systemProperty);
            return systemProperty;
        }
    }

    public static String determinePhantomJSBinaryPath(String modulePath) {
        String systemProperty = System.getProperty("headless.driver.binary");
        if (systemProperty == null || systemProperty.trim().length() == 0) {
            OSType osType = DetermineOS.getOperatingSystemType();

            switch (osType) {
                case WINDOWS:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/phantomjs-2-0-0.exe";
                case MACOS:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/phantomjs-macosx-2-0-0";
                case LINUX:
                    return getModuleDirectory(modulePath) + "/target/drivers/" + DRIVERS_LOCATION + "/phantomjs-linux-1-9-2";
                default:
                    LOG.severe("Unsupported operating system, provide the binary path yourself");
                    break;
            }

            return null;
        } else {
            LOG.info("Use the following binary for headless testing " + systemProperty);
            return systemProperty;
        }
    }

    /**
     * Running tests command line or from an IDE differs in determining the directory you are currently in.
     *
     * There you have to specify the relative path from the root of your project, where the automated tests should be ran.
     *
     * This checks is done OS independently, there providing the path can be with forward or backward slashes.
     *
     * @param modulePath
     * @return
     */
    public static String getModuleDirectory(String modulePath) {
        String root = System.getProperty("user.dir");

        String strippedRoot = root.replace("\\", "").replace("/", "");
        String strippedModulePath = modulePath.replace("\\", "").replace("/", "");
        if (strippedRoot.indexOf(strippedModulePath) < 0) {
            root = root + modulePath;
        }

        return root;
    }

    public static String getModuleDirectory() {
        return System.getProperty("user.dir");
    }
}
