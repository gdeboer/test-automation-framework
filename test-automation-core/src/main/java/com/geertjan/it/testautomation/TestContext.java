package com.geertjan.it.testautomation;

import com.geertjan.it.testautomation.driver.Runner;
import com.geertjan.it.testautomation.driver.RunnerFactory;
import com.geertjan.it.testautomation.driver.UserAgent;
import org.openqa.selenium.io.TemporaryFilesystem;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestContext {
    private static final Logger LOG = Logger.getLogger(TestContext.class.getName());
    private Runner runner;

    public TestContext(final Platform platform, final String mavenArtifactDirectory) {
        runner = RunnerFactory.getRunner(platform, null, mavenArtifactDirectory);
    }

    public TestContext(final Platform platform, final UserAgent userAgent, final String mavenArtifactDirectory) {
        runner = RunnerFactory.getRunner(platform, userAgent, mavenArtifactDirectory);
    }

    public void doTearDown() {
        TemporaryFilesystem tempFS = TemporaryFilesystem.getDefaultTmpFS();
        tempFS.deleteTemporaryFiles();

        destroyDriver();
    }

    public Runner getRunner() {
        return runner;
    }

    /**
     * The method to quit the browser or application under test and to empty the driver object. Useful for ending every test.
     *
     */
    private void destroyDriver() {
        if (runner != null) {
            try {
                runner.quit();
                runner = null;
            } catch (Exception exception) {
                LOG.log(Level.SEVERE, "Could not quit " + exception.getMessage());
            }
        }
    }

}