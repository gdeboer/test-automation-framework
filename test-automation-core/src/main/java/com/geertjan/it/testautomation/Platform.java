package com.geertjan.it.testautomation;

public enum Platform {
    FIREFOX, FIREFOX_LOCAL, CHROME, INTERNETEXPLORER, SAFARI, HEADLESS, ANDROID, IOS
}