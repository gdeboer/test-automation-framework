package com.geertjan.it.testautomation.driver;

import com.geertjan.it.testautomation.Platform;
import com.geertjan.it.testautomation.util.Locator;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RunnerFactory {
    private static Runner runner;
    private final static String GRID_URL = "http://continuous.geertjan.it/wd/hub";
    public final static String APPIUM_SERVER_URL_LOCAL = "http://localhost:4723/wd/hub";

    private static final Logger LOG = Logger.getLogger(RunnerFactory.class.getName());

    public static Runner getRunner(final Platform platform, final UserAgent userAgent, final String mavenArtifactDirectory) {
        try {
            switch(platform) {
                case FIREFOX_LOCAL:
                    FirefoxProfile fireFoxProfileLocal = new FirefoxProfile();
                    fireFoxProfileLocal.setPreference("webdriver_enable_native_events", true);

                    if(userAgent != null) {
                        fireFoxProfileLocal.setPreference("general.useragent.override", userAgent.toString());
                    }

                    // Hide infobar for missing plugins
                    fireFoxProfileLocal.setPreference("plugins.hide_infobar_for_missing_plugin", true);

                    runner = new Runner(new FirefoxDriver(fireFoxProfileLocal));
                    runner.manage().window().maximize();

                    break;
                case FIREFOX:
                    FirefoxProfile firefoxProfile = new FirefoxProfile();
                    firefoxProfile.setPreference("webdriver_enable_native_events", true);
                    if(userAgent != null) {
                        firefoxProfile.setPreference("general.useragent.override", userAgent.toString());
                    }

                    // Hide infobar for missing plugins
                    firefoxProfile.setPreference("plugins.hide_infobar_for_missing_plugin", true);

                    DesiredCapabilities capabilitiesFF = DesiredCapabilities.firefox();
                    capabilitiesFF.setCapability(FirefoxDriver.PROFILE, firefoxProfile);

                    runner = new Runner(new URL(GRID_URL), capabilitiesFF);
                    runner.manage().window().maximize();

                    break;
                case HEADLESS:
//                    runner = new Runner(new URL(GRID_URL), DesiredCapabilities.phantomjs());

                    String PHANTOM_JS_PATH = Locator.determinePhantomJSBinaryPath(mavenArtifactDirectory);

                    DesiredCapabilities desiredCapabilities = DesiredCapabilities.phantomjs();

                    if(userAgent != null) {
                        desiredCapabilities.setCapability("phantomjs.page.settings.userAgent",userAgent);
                    }

                    desiredCapabilities.setCapability("phantomjs.binary.path", PHANTOM_JS_PATH);
                    desiredCapabilities.setJavascriptEnabled(true);
                    desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

                    runner = new Runner(new FixedPhantomJSDriver(desiredCapabilities));

                    break;
                case CHROME:
                    DesiredCapabilities capabilitiesChrome = DesiredCapabilities.chrome();
                    ChromeOptions chromeOptions = new ChromeOptions();
                    if(userAgent != null) {
                        chromeOptions.addArguments("--user-agent=" + userAgent);
                    }
                    capabilitiesChrome.setCapability(ChromeOptions.CAPABILITY , chromeOptions);

                    runner = new Runner(new URL(GRID_URL), capabilitiesChrome);
                    runner.manage().window().maximize();

                    break;
                case INTERNETEXPLORER:
                    System.setProperty("webdriver.ie.driver", Locator.determineIEDriverBinaryPath(mavenArtifactDirectory));
                    runner = new Runner(new InternetExplorerDriver());
                    runner.manage().window().maximize();

                    break;
                case ANDROID:
                    throw new UnsupportedOperationException(String.format("Platform %s not supported yet ", platform));
//                    break;
                case IOS:
                    DesiredCapabilities desiredCapabilitiesIos = setCapabilities("deviceName=iPhone 6s Plus;platformVersion=9.0;browserName=Safari");
                    try {
                        runner = new Runner(new IOSDriver(new URL(APPIUM_SERVER_URL_LOCAL), desiredCapabilitiesIos));
                    } catch(ClassCastException e) {
                        LOG.log(Level.SEVERE, "", e);
                        throw new IllegalArgumentException("Could not connect to the grid, the url is probably wrong " + APPIUM_SERVER_URL_LOCAL);
                    } catch(Exception e) {
                        LOG.log(Level.SEVERE, "", e);
                        throw new IllegalArgumentException("Error while loading driver");
                    }

                    break;
                default:
                    throw new IllegalArgumentException("Unknown platform " + platform);
            }

            runner.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            return runner;
        } catch(MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Setter to initialize a {@link DesiredCapabilities} object using a String as input.
     *
     * @param capString A string that contains capability=value combinations, separated by a semicolon (;).
     * @return {@link DesiredCapabilities} instance, initialized with the values from the capString.
     */
    private static DesiredCapabilities setCapabilities(String capString) {
        DesiredCapabilities caps = new DesiredCapabilities();
        for (String keyValue : capString.split(";")) {
            caps.setCapability(keyValue.substring(0,keyValue.indexOf("=")),keyValue.substring(keyValue.indexOf("=")+1,keyValue.length()));
        }
        return caps;
    }

}


