package com.geertjan.it.testautomation.driver;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.Response;
import org.openqa.selenium.remote.UnreachableBrowserException;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * See
 * http://matejtymes.blogspot.co.uk/2014/10/webdriver-fix-for-unreachablebrowserexc.html
 *
 * and
 *
 * https://gist.github.com/rippo/7356507
 *
 */
public class FixedPhantomJSDriver extends PhantomJSDriver {
    private static final Logger LOG = Logger.getLogger(FixedPhantomJSDriver.class.getName());

    private final int retryCount = 5;

    public FixedPhantomJSDriver(Capabilities desiredCapabilities) {
        super(desiredCapabilities);
    }

    @Override
    protected Response execute(String driverCommand, Map<String, ?> parameters) {
        int retryAttempt = 0;

        while (true) {
            try {
                return super.execute(driverCommand, parameters);
            } catch (UnreachableBrowserException e) {
                LOG.log(Level.SEVERE, "Retry attempt " + retryAttempt + " " + e.getMessage());
                retryAttempt++;
                if (retryAttempt > retryCount) {
                    throw e;
                }
            }
        }
    }
}