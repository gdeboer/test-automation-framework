package com.geertjan.it.testautomation.driver;

import com.geertjan.it.testautomation.model.OSType;
import com.geertjan.it.testautomation.util.DetermineOS;
import com.geertjan.it.testautomation.util.Locator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.internal.FindsById;
import org.openqa.selenium.internal.FindsByLinkText;
import org.openqa.selenium.internal.FindsByName;
import org.openqa.selenium.internal.FindsByTagName;
import org.openqa.selenium.internal.FindsByXPath;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.fail;

public class Runner implements WebDriver, JavascriptExecutor,
        FindsById, FindsByClassName, FindsByLinkText, FindsByName,
        FindsByCssSelector, FindsByTagName, FindsByXPath,
        HasInputDevices, HasCapabilities, TakesScreenshot {
    private static final Logger LOG = Logger.getLogger(Runner.class.getName());
    private int WAIT_FOR_EXPECTED_CONDITION_TIMEOUT_IN_SECONDS = 10;
    private RemoteWebDriver webDriver;
    private OSType osType;

    public Runner(URL remoteAddress, Capabilities desiredCapabilities) {
        webDriver = new RemoteWebDriver(remoteAddress, desiredCapabilities);
    }

    public Runner (RemoteWebDriver remoteWebDriver) {
        this.webDriver = remoteWebDriver;
        this.osType = DetermineOS.getOperatingSystemType();
    }

    /**
     * The osType needs to be the OS on which the tests are ran.
     *
     * So, if running the tests from the buildserver [Windows] against the T environment [Unix]: this variable is Windows.
     *
     */
    public void takeAndSaveScreenshot() {
        String root = Locator.getModuleDirectory();

        File screenShot = null;
        if(webDriver != null && webDriver instanceof PhantomJSDriver) {
            screenShot = ((PhantomJSDriver) webDriver).getScreenshotAs(OutputType.FILE);
        }

        if(webDriver != null) {
            screenShot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        }

        if(screenShot != null) {
            DateTime dateTime = DateTime.now();
            org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("YYDDDHHmmssSS");
            String timeFormatted = formatter.print(dateTime);

            String filepath = "";
            switch (osType) {
                case WINDOWS:
                    filepath = root + "\\target\\" + timeFormatted + ".png";
                    break;
                case MACOS:
                    filepath = root + "/target/" + timeFormatted + ".png";
                    break;
                case LINUX:
                    filepath = root + "/target/" + timeFormatted + ".png";
                    break;
                default:
                    throw new RuntimeException("Unsupported operating system!");
            }

            try {
                FileUtils.copyFile(screenShot, new File(filepath));
            } catch (Exception e) {
                LOG.log(Level.SEVERE, "Could not write the screenshot to " + filepath + " on os type " + osType, e);
            }
        }
    }

    // USER ACTIONS
    public void setValueById(String id, String value) {
        WebElement elem = webDriver.findElement(By.id(id));
        elem.clear();
        elem.sendKeys(value);
    }

    public void setValueByName(String id, String value) {
        WebElement elem = webDriver.findElement(By.name(id));
        elem.clear();
        elem.sendKeys(value);
    }

    public void setValueBySelector(By selector, String value) {
        WebElement webElement = webDriver.findElement(selector);
        webElement.clear();
        webElement.sendKeys(value);
    }

    public void clickButtonById(String id) {
        WebElement elem = webDriver.findElement(By.id(id));
        elem.click();
    }

    public void clickButtonByClassName(String className) {
        WebElement elem = webDriver.findElement(By.className(className));
        elem.click();
    }

    public void clickButtonByName(String name) {
        WebElement elem = webDriver.findElement(By.name(name));
        elem.click();
    }

    public void waitFor(ExpectedCondition<?> expectedCondition) {
        try {
            (new WebDriverWait(webDriver, WAIT_FOR_EXPECTED_CONDITION_TIMEOUT_IN_SECONDS)).until(expectedCondition);
        } catch(TimeoutException timeoutException) {
            takeAndSaveScreenshot();
            fail(String.format("Did not meet expected condition with %s seconds, received the following timeout message %s", WAIT_FOR_EXPECTED_CONDITION_TIMEOUT_IN_SECONDS, timeoutException.getMessage()));
        }
    }

    public void waitForTextPresent(By selector, String text) {
        waitFor(ExpectedConditions.textToBePresentInElementLocated(selector, text));
    }

    /**
     * Set the selected option of a dropdown by the provided visible text
     *
     * @param name
     * @param visibleText
     */
    public void selectDropdownOptionByVisibleTextAndName(String name, String visibleText) {
        new Select(webDriver.findElement(By.name(name))).selectByVisibleText(visibleText);
    }

    public void selectDropdownOptionByVisibleTextAndId(String id, String visibleText) {
        new Select(webDriver.findElement(By.id(id))).selectByVisibleText(visibleText);
    }

    public void selectOptionRadiobuttonByValue(final String name, final String value) {
        if(StringUtils.isEmpty(value) || StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException(String.format("selectOptionRadiobuttonByValue invalid arguments %s %s", name, value));
        }

        List<WebElement> radios = webDriver.findElements(By.name(name));
        for(WebElement radio: radios) {
            if(value.equals(radio.getAttribute("value"))) {
                while(!radio.isSelected()) {
                    radio.click();
                }
                return;
            }
        }

        fail(String.format("The value %s for radio %s was not found", value, name));
    }

    public void clickRadioOptionById(String id) {
        WebElement webElement = webDriver.findElement(By.id(id));
        webElement.click();
    }

    public void clickCheckboxByName(String name) {
        WebElement webElement = webDriver.findElement(By.name(name));
        webElement.click();
    }

    public void pressBackButton() {
        webDriver.navigate().back();
    }

    // VERIFICATIONS
    /**
     * Verifies the given text is present in the source of the page.
     *
     * @param text
     * @throws java.lang.AssertionError if the text is not present
     */
    public void verifyTextIsPresentOnThePage(String text) {
        String trimmedPageSource = webDriver.getPageSource().replaceAll("\\s+", " ");
        if(!trimmedPageSource.contains(text)) {
            takeAndSaveScreenshot();
            fail("The following text expected on the page, but was NOT found " + text);
        }
    }

    /**
     * Verifies the given text is NOT present in the source of the page.
     *
     * @param text
     * @throws java.lang.AssertionError if the text is present
     */
    public void verifyTextIsNotPresentOnThePage(String text) {
        String trimmedPageSource = webDriver.getPageSource().replaceAll("\\s+", " ");

        if(trimmedPageSource.contains(text)) {
            takeAndSaveScreenshot();
            fail("The following text was NOT expected on the page, but was found " + text);
        }
    }

    public void verifyPageAHrefLink(String visibleLinkText, String link) {
        WebElement element = webDriver.findElement(By.linkText(visibleLinkText));
        String actualLink = element.getAttribute("href");
        if(!actualLink.equals(link)) {
            takeAndSaveScreenshot();
            fail(String.format("The a href link with visible text %s should be %s, but was %s", visibleLinkText, link, actualLink));
        }
    }

    public void verifyCookieNotPresent(String cookieName) {
        Cookie cookie = webDriver.manage().getCookieNamed(cookieName);
        if(cookie != null) {
            takeAndSaveScreenshot();
            fail(String.format("A cookie with name %s was not expected, but was found", cookieName));
        }
    }

    public void verifyCookie(String cookieName, String containsValue) {
        Cookie cookie = webDriver.manage().getCookieNamed(cookieName);
        if(cookie == null) {
            takeAndSaveScreenshot();
            fail(String.format("A cookie with name %s was expected, but was not found", cookieName));
        }

        if(!cookie.getValue().contains(containsValue)) {
            takeAndSaveScreenshot();
            fail(String.format("The cookie with name %s should contain the value %s but it did NOT, the value of the cookie was %s", cookieName, containsValue, cookie.getValue()));
        }
    }

    public void verifyUrl(String expectedUrl, String actualUrl) {
        if(!actualUrl.equals(expectedUrl)) {
            fail(String.format("The actual url %s did not meet the expected url %s", actualUrl, expectedUrl));
        }
    }

    public void verifyUrlContainsParameterWithValue(String url, String requiredParameter, String requiredParameterValue) {
        if(url == null) {
            fail(String.format("The url is null, so could not verify it contains parameter %s with value %s", requiredParameter, requiredParameterValue));
        }

        List<NameValuePair> params;
        try {
            params = URLEncodedUtils.parse(new URI(url), "UTF-8");

            for (NameValuePair param : params) {
                if(requiredParameter.equals(param.getName()) && requiredParameterValue.equals(param.getValue())) {
                    return;
                }
            }

        } catch (URISyntaxException e) {
            throw new RuntimeException("Could not parse url " + url);
        }

        fail(String.format("The parameter %s with value %s was not found within the url %s", requiredParameter, requiredParameterValue, url));
    }

    public void verifyCheckboxIsNotSelectedByName(String name) {
        boolean isSelected = webDriver.findElement(By.id(name)).isSelected();
        if(isSelected) {
            takeAndSaveScreenshot();
            fail(String.format("The checkbox with name %s should not be selected but is", name));
        }
    }

    public void verifyCheckboxIsSelectedByName(String name) {
        boolean isSelected = webDriver.findElement(By.name(name)).isSelected();
        if(!isSelected) {
            takeAndSaveScreenshot();
            fail(String.format("The checkbox with name %s should be selected but is not", name));
        }
    }

    public void verifyXmlHasElementWithValue(String xml, boolean ignoreNamespace, String expectedElement, String expectedValue) {
        String actualValue = "";
        if(ignoreNamespace) {
            if(StringUtils.contains(xml, "<" + expectedElement)) {
                actualValue = StringUtils.substringBetween(xml, "<" + expectedElement + StringUtils.substringBetween(xml, "<" + expectedElement, ">") + ">", "</");
            } else {
                actualValue = StringUtils.substringBetween(xml, expectedElement + ">", "</");
            }
        } else {
            actualValue = StringUtils.substringBetween(xml, "<" + expectedElement + ">", "</" + expectedElement + ">");
        }

        if(actualValue == null) {
            takeAndSaveScreenshot();
            fail(String.format("The xml %s should have contained an element %s but this was not found", xml, expectedElement));
        }

        if(!expectedValue.equals(actualValue)) {
            takeAndSaveScreenshot();
            fail(String.format("The xml %s should have contained an element %s with value %s but the actual value %s was found", xml, expectedElement, expectedValue, actualValue));
        }
    }

    public void verifyValueByName(String name, String value) {
        String actualValue = getValueByName(name);
        if(!value.equals(actualValue)) {
            takeAndSaveScreenshot();
            fail(String.format("The input field with name %s should have value %s but was %s", name, value, actualValue));
        }
    }

    public void verifyValueById(String id, String value) {
        String actualValue = getValueById(id);
        if(!value.equals(actualValue)) {
            takeAndSaveScreenshot();
            fail(String.format("The input field with id %s should have value %s but was %s", id, value, actualValue));
        }
    }

    public void verifyRadioIsSelectedByName(String name) {
        boolean isSelected = webDriver.findElement(By.id(name)).isSelected();
        if(!isSelected) {
            takeAndSaveScreenshot();
            fail(String.format("The checkbox with name %s should be selected but is not", name));
        }
    }

    public void verifyRadioIsNotSelectedByName(String name) {
        boolean isSelected = webDriver.findElement(By.id(name)).isSelected();
        if(isSelected) {
            takeAndSaveScreenshot();
            fail(String.format("The checkbox with name %s should not be selected but is", name));
        }
    }

    public void verifyComponentDisplayedById(String id) {
        boolean isAvailable = webDriver.findElement(By.id(id)).isDisplayed();
        if(!isAvailable){
            takeAndSaveScreenshot();
            fail(String.format("The UI component is not available %s", id));
        }
    }

    public void verifyComponentNotDisplayedById(String id) {
        try {
            verifyComponentDisplayedById(id);
            takeAndSaveScreenshot();
            fail(String.format("The UI component should not be available %s", id));
        } catch(NoSuchElementException noSuchElementException) {
            return;
        }
    }

    public String getValueByName(String name) {
        try {
            return webDriver.findElement(By.name(name)).getAttribute("value");
        } catch(NoSuchElementException noSuchElementException) {
            takeAndSaveScreenshot();
            fail(String.format("Element with name %s not found", name));
            return null;
        }
    }

    public String getValueById(String id) {
        try {
            return webDriver.findElement(By.id(id)).getAttribute("value");
        } catch(NoSuchElementException noSuchElementException) {
            takeAndSaveScreenshot();
            fail(String.format("Element with id %s not found", id));
            return null;
        }
    }

    // OVERRIDES

    @Override
    public void get(String s) {
        webDriver.get(s);
    }

    @Override
    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return webDriver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return webDriver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return webDriver.findElement(by);
    }

    @Override
    public String getPageSource() {
        return webDriver.getPageSource();
    }

    @Override
    public void close() {
        webDriver.close();
    }

    @Override
    public void quit() {
        webDriver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return webDriver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return webDriver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return webDriver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return webDriver.navigate();
    }

    @Override
    public Options manage() {
        return webDriver.manage();
    }

    @Override
    public Keyboard getKeyboard() {
        return ((HasInputDevices) webDriver).getKeyboard();
    }

    @Override
    public Mouse getMouse() {
        return ((HasInputDevices) webDriver).getMouse();
    }

    @Override
    public WebElement findElementByClassName(String using) {
        return webDriver.findElementByClassName(using);
    }

    @Override
    public List<WebElement> findElementsByClassName(String using) {
        return webDriver.findElementsByClassName(using);
    }

    @Override
    public WebElement findElementByCssSelector(String using) {
        return webDriver.findElementByCssSelector(using);
    }

    @Override
    public List<WebElement> findElementsByCssSelector(String using) {
        return webDriver.findElementsByCssSelector(using);
    }

    @Override
    public WebElement findElementById(String using) {
        return webDriver.findElementById(using);
    }

    @Override
    public List<WebElement> findElementsById(String using) {
        return webDriver.findElementsById(using);
    }

    @Override
    public WebElement findElementByLinkText(String using) {
        return webDriver.findElementByLinkText(using);
    }

    @Override
    public List<WebElement> findElementsByLinkText(String using) {
        return webDriver.findElementsByLinkText(using);
    }

    @Override
    public WebElement findElementByPartialLinkText(String using) {
        return webDriver.findElementByPartialLinkText(using);
    }

    @Override
    public List<WebElement> findElementsByPartialLinkText(String using) {
        return webDriver.findElementsByPartialLinkText(using);
    }

    @Override
    public WebElement findElementByName(String using) {
        return webDriver.findElementByName(using);
    }

    @Override
    public List<WebElement> findElementsByName(String using) {
        return webDriver.findElementsByName(using);
    }

    @Override
    public WebElement findElementByTagName(String using) {
        return webDriver.findElementByTagName(using);
    }

    @Override
    public List<WebElement> findElementsByTagName(String using) {
        return webDriver.findElementsByTagName(using);
    }

    @Override
    public WebElement findElementByXPath(String using) {
        return webDriver.findElementByXPath(using);
    }

    @Override
    public List<WebElement> findElementsByXPath(String using) {
        return webDriver.findElementsByXPath(using);
    }

    @Override
    public Capabilities getCapabilities() {
        return webDriver.getCapabilities();
    }

    @Override
    public Object executeScript(String script, Object... args) {
        return webDriver.executeScript(script, args);
    }

    @Override
    public Object executeAsyncScript(String script, Object... args) {
        return webDriver.executeAsyncScript(script, args);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return webDriver.getScreenshotAs(target);
    }
}
