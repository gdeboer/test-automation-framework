package com.geertjan.it.testautomation.model;

public enum OSType {
    WINDOWS, MACOS, LINUX
}
