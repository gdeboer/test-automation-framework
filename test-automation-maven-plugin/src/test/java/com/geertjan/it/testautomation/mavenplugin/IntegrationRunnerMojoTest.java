package com.geertjan.it.testautomation.mavenplugin;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;

public class IntegrationRunnerMojoTest {

    @Mock
    private MavenProject mavenProject;

    @Mock
    private MavenSession mavenSession;

    @Mock
    private BuildPluginManager pluginManager;

    @Test
    public void test() throws MojoExecutionException {
        mavenProject = mock(MavenProject.class);
        mavenSession = mock(MavenSession.class);
        pluginManager = mock(BuildPluginManager.class);

        IntegrationRunnerMojo integrationRunnerMojo = new IntegrationRunnerMojo();

        integrationRunnerMojo.setMavenProject(mavenProject);
        integrationRunnerMojo.setMavenSession(mavenSession);
        integrationRunnerMojo.setPluginManager(pluginManager);

//        integrationRunnerMojo.execute();
    }
}