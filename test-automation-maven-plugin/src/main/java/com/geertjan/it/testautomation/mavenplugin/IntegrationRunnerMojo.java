package com.geertjan.it.testautomation.mavenplugin;

import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import static org.twdata.maven.mojoexecutor.MojoExecutor.artifactId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.attribute;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.dependencies;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.executeMojo;
import static org.twdata.maven.mojoexecutor.MojoExecutor.executionEnvironment;
import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;
import static org.twdata.maven.mojoexecutor.MojoExecutor.groupId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;
import static org.twdata.maven.mojoexecutor.MojoExecutor.plugin;
import static org.twdata.maven.mojoexecutor.MojoExecutor.version;

/**
 * Mojo to run the integration test mojo's via the mojo executor of https://github.com/TimMoore/mojo-executor
 *
 */
@Mojo( name = "integration", requiresDependencyCollection = ResolutionScope.COMPILE)
public class IntegrationRunnerMojo extends AbstractRunnerMojo {
    @Parameter(defaultValue = "**/*.java" )
    private String testsToRun;

    public void execute() throws MojoExecutionException {
        getLog().info("Start integration test");

        getLog().info("Download drivers");
        downloadDrivers();

        getLog().info("Change file permissions for the drivers");
        changeFileRights();

        getLog().info("Execute the integration tests for tests matching " + testsToRun);
        executeFailsafePlugin();
    }

    /**
     *         <plugin>
     *          <groupId>org.apache.maven.plugins</groupId>
     *          <artifactId>maven-dependency-plugin</artifactId>
     *          <version>2.7</version>
     *          <executions>
     *              <execution>
     *                  <id>unpack-drivers</id>
     *                  <phase>compile</phase>
     *                  <goals>
     *                      <goal>unpack-dependencies</goal>
     *                  </goals>
     *                  <configuration>
     *                      <includeGroupIds>com.geertjan.it.testautomation</includeGroupIds>
     *                      <includeArtifactIds>test-automation-drivers</includeArtifactIds>
     *                      <outputDirectory>${project.build.directory}/drivers</outputDirectory>
     *                  </configuration>
     *              </execution>
     *          </executions>
     *         </plugin>
     *
     * @throws MojoExecutionException
     */
    private void downloadDrivers() throws MojoExecutionException {

                executeMojo(
                        plugin(
                                groupId("org.apache.maven.plugins"),
                                artifactId("maven-dependency-plugin"),
                                version("2.7")
                        ),
                        goal("unpack-dependencies"),
                        configuration(
                                element(name("includeGroupIds"), "com.geertjan.it.testautomation"),
                                element(name("includeArtifactIds"), "test-automation-drivers"),
                                element(name("outputDirectory"), "${project.build.directory}/drivers")
                        ),
                        executionEnvironment(
                                getMavenProject(),
                                getMavenSession(),
                                getPluginManager()
                        )
                );
    }

    /**
      * We need the following to set filemode for linux executables:
      *
      *        <tasks>
      *          <chmod perm="u+x">
      *              <fileset dir="${project.build.directory}/drivers">
      *                  <include name="**" />
      *              </fileset>
      *          </chmod>
      *        </tasks>
      *
      * @throws MojoExecutionException
      */
    private void changeFileRights() throws MojoExecutionException {

        MojoExecutor.Element include = element(name("include"), attribute("name", "**/*"));
        MojoExecutor.Element fileset = element(name("fileset"), attribute("dir", "${project.build.directory}/drivers"), include);
        MojoExecutor.Element chmod = element(name("chmod"), attribute("perm", "u+x"), fileset);
        MojoExecutor.Element tasks = element(name("tasks"), chmod);

        executeMojo(
                plugin(
                        groupId("org.apache.maven.plugins"),
                        artifactId("maven-antrun-plugin"),
                        version("1.7")
                ),
                goal("run"),
                configuration(
                        tasks
                ),
                executionEnvironment(
                        getMavenProject(),
                        getMavenSession(),
                        getPluginManager()
                )
        );
    }

    /**
     * <plugin>
     * <groupId>org.apache.maven.plugins</groupId>
     * <artifactId>maven-failsafe-plugin</artifactId>
     * <version>2.18.1</version>
     * <dependencies>
     * <dependency>
     * <groupId>org.apache.maven.surefire</groupId>
     * <artifactId>surefire-junit47</artifactId>
     * <version>2.18.1</version>
     * </dependency>
     * </dependencies>
     * <configuration>
     * <includes>
     * <include><<includes>></include>
     * </includes>
     * </configuration>
     * <executions>
     * <execution>
     * <goals>
     * <goal>integration-test</goal>
     * <goal>verify</goal>
     * </goals>
     * </execution>
     * </executions>
     * </plugin>
     *
     * @throws MojoExecutionException
     */
    private void executeFailsafePlugin() throws MojoExecutionException {
        Dependency dependency = new Dependency();
        dependency.setGroupId("org.apache.maven.surefire");
        dependency.setArtifactId("surefire-junit47");
        dependency.setVersion("2.18.1");

        MojoExecutor.Element include = element(name("include"), testsToRun);
        MojoExecutor.Element includes = element(name("includes"), include);

        executeMojo(
                plugin(
                        groupId("org.apache.maven.plugins"),
                        artifactId("maven-failsafe-plugin"),
                        version("2.18.1"),
                        dependencies(dependency)
                ),
                goal("integration-test"),
                configuration(
                        includes
                ),
                executionEnvironment(
                        getMavenProject(),
                        getMavenSession(),
                        getPluginManager()
                )
        );

        executeMojo(
                plugin(
                        groupId("org.apache.maven.plugins"),
                        artifactId("maven-failsafe-plugin"),
                        version("2.18.1"),
                        dependencies(dependency)
                ),
                goal("verify"),
                configuration(),
                executionEnvironment(
                        getMavenProject(),
                        getMavenSession(),
                        getPluginManager()
                )
        );

    }
}